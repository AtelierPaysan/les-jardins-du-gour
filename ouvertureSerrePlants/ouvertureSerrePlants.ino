/*
Ouverture serre plants

Ouvre la serre selon température et hygrométrie.
Allume le chauffage si trop froid.
Enregistre température et hygrométrie.


Tous les contacts doivent être pullup
 */

//  -----liste des librairies
// DS 18b20
#include <OneWire.h> //Librairie du bus OneWire
#include <DallasTemperature.h> //Librairie du capteur 18b20
// Dht 
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
// Uncomment the type of sensor in use:
#define DHTTYPE           DHT11     // DHT 11 
//#define DHTTYPE           DHT22     // DHT 22 (AM2302)
//#define DHTTYPE           DHT21     // DHT 21 (AM2301)

// Écran 1602 avec 74HC595
#include <SPI.h>
#include <LiquidCrystal.h>

#include <ClickEncoder.h>  // lib encodeur
#include <TimerOne.h> // lib TimerOne
//eeprom
#include <EEPROM.h>


// -----liste des pins connectés:

// initialize the library with the number of the sspin 
//(or the latch pin of the 74HC595)
LiquidCrystal lcd(10);  //utilise 10 11 13

#define pinClk  A1  // encodeur rotatif: clk
#define pinDt   A0  // encodeur rotatif: dt
#define pinSw   A2  // encodeur rotatif: bouton

#define pinOuv  7  // contact ouverture
#define pinFerm 8  // contact fermeture

OneWire oneWire(3); // pin Bus One Wire pour capteur température 18b20
DallasTemperature sensors(&oneWire); //Utilistion du bus Onewire pour les capteurs
DeviceAddress sensorDeviceAddress; //Vérifie la compatibilité des capteurs avec la librairie
#define DHTPIN  2   // capteur hygrométrie dht 11 ou ...

#define pinSChauff 4  // sortie chauffage

#define pinSMoteurOuv  5  // sortie moteur ouverture
#define pinSMoteurFerm 6  // sortie moteur fermeture
#define pinPower 9  // sortie alim atx

//dht
DHT_Unified dht(DHTPIN, DHTTYPE);
uint32_t delayMS;

//-------liste des variables / constantes

//config a la compil
const long timoutMoteur = 3000 ; // temps maxi marche moteurs en milisecondes 120000
const int nbPas = 8 ; // nombre de pas d'ouverture/fermeture
const long tIdle = 30000  ; //Temps d'une boucle entre deux mesures en milisecondes 300000
const long timeMaxiChauf = 1380 ; // temps avant reset du chauffage minutes

/** Le nombre magique et le numéro de version actuelle pour eeprom*/
static const unsigned long STRUCT_MAGIC = 123456789;
static const byte STRUCT_VERSION = 0;

// -- Pour fonction Menu config
char* textMenuItem[] = {" ", "Mode Tempete", 
"Temp Ouverture",
"Temp fermeture",
"Hygro ouverture",
"Hygro t ouv max",
"Hygro t ferm mini",
"T Chauff On",
"T Chauff Off",
"Mesure moteurs",
"Reset",
"Sortie menu",
" ",
} ;

//config via menu
int tempete = 0;  // mode tempête désactivé/activé
int tempOuv = 40 ; // température d'ouverture
int tempFerm = 15 ; // température de fermeture
int hygroOuv = 80 ; // hygrométrie d'ouverture pour ventiler
int tempsOuvHygroMax = 5 ; // nombre de minutes / 10 maxi où l'on reste ouvert si trop d'hygro
int tempsFermHygroMini = 120 ; // nombre de minutes / 10 où l'on reste fermé entre deux ouvertures si trop d'hygro
int tempChauffOn = 13 ; // température allumage chauffage
int tempChauffOff = 25 ; // température arret chauffage

// ne pas toucher ces variables
int etat = 0 ; //variable d'état
//int tmp = 0 ; 	//variable temporaire
//int timer = 0 ; //variable temporisation
int ouvTime = 0 ; //temps d'ouverture totale milisecondes
int fermTime = 0 ; //temps de fermeture totale milisecondes
int pasOuv = 0 ; //temps d'un pas d'ouverture milisecondes
int pasFerm = 0 ; //temps d'un pas de fermeture milisecondes
float tempActu = 0 ; //température actuelle
float tempMini = 0 ; //température mini
float tempMaxi = 0 ; //température maxi
float hygroActu = 0; //hygrométrie actuelle
float hygroMini = 0; //hygrométrie mini
float hygroMaxi = 0; //hygrométrie maxi
int chauffOn = 0 ; // état chauffage: allumé = 1
int pasActuel = 0 ; // position d'ouverture
unsigned long timeFermHygro = 0 ; // compteur temps de fermeture si trop d'hygro
unsigned long timeOuvHygro = 0 ; // compteur temps de ouverture si trop d'hygro
int OuvHygro = 0 ; // ouvert par excès d'hygro 1 oui / 0 non
unsigned long timeChauf ; //heure mise en route chauffage en minutes
unsigned long lastEfface = 0 ; //enregistrement dernier effacement converti en s.
int NbAll = 0 ; //compteur nombre allumage chauffage
unsigned long HChauffOn = 0 ; // Compteurs d'heures de marche du chauffage en minutes

//liste des constantes
enum {OUV, FERM};	//NE pas toucher: pour gestion moteurs

// pour encodeur
ClickEncoder *encoder;
int16_t last, value;

void timerIsr() {
  encoder->service();
}

//pour eeprom
/** La structure qui contient les données */
struct ConfigData {
  unsigned long magic;
  byte struct_version;
  
  int tempete ;  // mode tempête désactivé/activé
  int tempOuv ; // température d'ouverture
  int tempFerm ; // température de fermeture
  int hygroOuv ; // hygrométrie d'ouverture pour ventiler
  int tempsOuvHygroMax ; // nombre de minutes / 10 maxi où l'on reste ouvert si trop d'hygro
  int tempsFermHygroMini ; // nombre de minutes / 10 où l'on reste fermé entre deux ouvertures si trop d'hygro
  int tempChauffOn ; // température allumage chauffage
  int tempChauffOff ; // température arret chauffage
  int ouvTime ;
  int fermTime ;
  int pasOuv ;
  int pasFerm ;
};
/** L'instance de la structure, globale, car utilisé dans plusieurs endroits du programme */
ConfigData cd;

// the setup routine runs once when you press reset:
void setup() {
 
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  
  // config des pins
  pinMode(pinOuv, INPUT_PULLUP);
  pinMode(pinFerm, INPUT_PULLUP);
  pinMode(pinSChauff, OUTPUT);
  pinMode(pinSMoteurOuv, OUTPUT);
  pinMode(pinSMoteurFerm, OUTPUT);
  pinMode(pinPower,OUTPUT);
  
// DS 18B20
sensors.begin(); //Activation des capteurs
sensors.getAddress(sensorDeviceAddress, 0); //Demande l'adresse du capteur à l'index 0 du bus
sensors.setResolution(sensorDeviceAddress, 12); //Résolutions possibles: 9,10,11,12

// Initialise dht
  dht.begin();
  sensor_t sensor;
      dht.humidity().getSensor(&sensor);

// dht: Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;
  
// initialise l'écran
     lcd.begin(16, 2);

    
  encoder = new ClickEncoder(pinClk, pinDt, pinSw);
  encoder->setAccelerationEnabled(false);
 

  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr); 
  
  last = encoder->getValue();
  
  //lecture / écriture eeprom
   // Charge le contenu de la mémoire
  chargeEEPROM();
    
}

// the loop routine runs over and over again forever:
void loop() {
//      	bitWrite(lcd._bitString, 0, LOW); // led éteinte
    	bitWrite(lcd._bitString, 2, LOW); // rétroéclairage éteint
    lcd.spiSendOut();
    
  switch (etat) {
    case 0:
      //Initialisation
      Serial.println("initmesures");
      etat = initMesures();
      break;
    case 1:
      //idle
      Serial.println("idle");
      etat = idle();
      break;
    case 2:
//mesure capteurs
Serial.println("mesures");
if (tempete == 0) etat = gestTempH() ;
if (tempete == 1) etat = actionTempete() ;
      break;
    case 3:
      //affichage mesures
      Serial.println("affich");
etat = affich(tempMini, tempMaxi, hygroMini, hygroMaxi) ;
      break;
    case 4:
      //config
      Serial.println("config");
etat = config() ;
      break;
    case 5:
      //Effacement
      Serial.println("efface");
      etat = efface() ;
      break;
    case 10:
      //init moteurs
      Serial.println("initmoteurs");
      etat = initMoteurs();
      Serial.print( "ouvTime:" );Serial.print( ouvTime ); Serial.print('/');Serial.print( fermTime ); Serial.print('/');Serial.print( pasOuv ); Serial.print('/');Serial.print( pasFerm );
      break;
    default: 
      // if nothing else matches, do the default
      // default is optional
      etat = 0 ;
  }


}

// Fonction initMoteurs
int initMoteurs ()
{
    bitWrite(lcd._bitString, 0, HIGH); //allume led
    lcd.spiSendOut();
  digitalWrite(pinPower, LOW);   // allumage alim.
    delay(500);

int timer = 0; // variable temporisation  
int tmp = digitalRead(pinFerm);   // lit contact de fermeture

// on ferme la serre si elle est ouverte
while(tmp > 0 && timer < timoutMoteur){
  digitalWrite(pinSMoteurFerm, HIGH);   // ferme la serre
  delay (1);
    timer ++ ;
  tmp = digitalRead(pinFerm);   // lit contact de fermeture
}
  digitalWrite(pinSMoteurFerm, LOW);   // coupe moteurs
timer = 0;
tmp = digitalRead(pinOuv);   // lit contact d'ouverture

// on ouvre la serre pour compter le temps d'ouverture
while(tmp > 0 && timer < timoutMoteur){
  digitalWrite(pinSMoteurOuv, HIGH);   // ouvre la serre
  delay (1);
  timer ++ ;
  ouvTime ++;
  tmp = digitalRead(pinOuv);   // lit contact d'ouverture
}
  digitalWrite(pinSMoteurOuv, LOW);   // coupe moteurs
timer = 0;
tmp = digitalRead(pinFerm);   // lit contact de fermeture

// on ferme la serre pour compter le temps de fermeture
while(tmp > 0 && timer < timoutMoteur){
  digitalWrite(pinSMoteurFerm, HIGH);   // ferme la serre
  delay (1);
  timer ++;
  fermTime ++;
  tmp = digitalRead(pinFerm);   // lit contact de fermeture

}
  digitalWrite(pinSMoteurFerm, LOW);   // coupe moteurs
tmp = 0;
timer = 0;

// calcul des pas
  
  pasOuv = ouvTime / nbPas;
  pasFerm = fermTime / nbPas;
  
    digitalWrite(pinPower, HIGH);   // arret alim.

    //Sauvegarde dans eeprom
    cd.ouvTime = ouvTime;
    cd.fermTime = fermTime;
    cd.pasOuv = pasOuv;
    cd.pasFerm = pasFerm;
    sauvegardeEEPROM();

    bitWrite(lcd._bitString, 0, LOW); //eteint led
    lcd.spiSendOut();
    
    pasActuel = 0 ;
    
    return(1);
}


// init mesures
int initMesures(){
  
      bitWrite(lcd._bitString, 0, HIGH); //allume led
    lcd.spiSendOut();

    digitalWrite(pinPower, LOW);   // allumage alim.
  delay(500);
int timer = 0; // variable temporisation  
int tmp = digitalRead(pinFerm);   // lit contact de fermeture

// on ferme la serre si elle est ouverte
while(tmp > 0 && timer < timoutMoteur){
  digitalWrite(pinSMoteurFerm, HIGH);   // ferme la serre
  delay (1);
    timer ++ ;
  tmp = digitalRead(pinFerm);   // lit contact de fermeture
}
  digitalWrite(pinSMoteurFerm, LOW);   // coupe moteurs
    pasActuel = 0 ;

      digitalWrite(pinPower, HIGH);   // arrêt alim.

  
  // init valeurs température hygrométrie
  tmp = 0;

  float temp0 = 0;
  float tempMesure = 0;
  float hygroMesure = 0;
  float hygro = 0 ;
  
  while(tmp < 5) {
  // mesure température
//ds18b20
  sensors.requestTemperatures(); //Demande la température aux capteurs
 temp0= sensors.getTempCByIndex(0); //Récupération de la température en celsius du capteur n°0

 // moyenne des mesures
 if (tempMesure == 0 && temp0 != 0) tempMesure = temp0 ;
tempMesure = (temp0 + tempMesure)/2 ;
 
 
 // dht
 // Delay between measurements.
  delay(delayMS);
  sensors_event_t event;  
 // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println("Error reading humidity!");
  }
  else {
    hygro = event.relative_humidity;
  }

  // moyenne des mesures
   if (hygroMesure == 0 && hygro != 0) hygroMesure = hygro ;
hygroMesure = (hygro + hygroMesure)/2 ;

tmp ++ ;
  }

//enregistrement valeurs mini/maxi/actuelle

tempActu = tempMesure;
tempMini = tempMesure;
tempMaxi = tempMesure;

hygroActu = hygroMesure;
hygroMini = hygroMesure;
hygroMaxi = hygroMesure;

      bitWrite(lcd._bitString, 0, LOW); //eteint led
    lcd.spiSendOut();

// envoi vers idle:
return(1) ;    
}

//Fonction mesures
void mesures(){
     bitWrite(lcd._bitString, 0, HIGH); //allumage led
    lcd.spiSendOut();
  int tmp = 0;

  float temp0 = 0;
  float tempMesure = 0;
  float hygroMesure = 0;
  float hygro = 0 ;
  
  while(tmp < 5) {
  // mesure température
//ds18b20
  sensors.requestTemperatures(); //Demande la température aux capteurs
 temp0= sensors.getTempCByIndex(0); //Récupération de la température en celsius du capteur n°0

 // moyenne des mesures
 if (tempMesure == 0 && temp0 != 0) tempMesure = temp0 ;
tempMesure = (temp0 + tempMesure)/2 ;
 
 
 // dht
 // Delay between measurements.
  delay(delayMS);
  sensors_event_t event;  
 // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println("Error reading humidity!");
  }
  else {
    hygro = event.relative_humidity;
  }

  // moyenne des mesures
   if (hygroMesure == 0 && hygro != 0) hygroMesure = hygro ;
hygroMesure = (hygro + hygroMesure)/2 ;

tmp ++ ;
  }

//enregistrement valeurs mini/maxi/actuelle

tempActu = tempMesure;
if (tempMesure < tempMini) tempMini = tempMesure ;
if (tempMesure > tempMaxi) tempMaxi = tempMesure ;

hygroActu = hygroMesure;
if (hygroMesure < hygroMini) hygroMini = hygroMesure ;
if (hygroMesure > hygroMaxi) hygroMaxi = hygroMesure ;
   
    bitWrite(lcd._bitString, 0, LOW); //eteint led
    lcd.spiSendOut();

}


// Fonction gestTempH gère température et hygrométrie
int gestTempH() {

  mesures();
      unsigned long time = millis();
    time = time / 60000; //temps en minutes

    if (hygroActu < hygroOuv && OuvHygro == 1){ // si on était ouvert pour trop d'hygro et que l'hygro a baissé
      OuvHygro = 0;
      timeFermHygro = time;
    }
    
//test si chauffage en marche
if (chauffOn ==1)
{
  if (tempActu > tempChauffOff) //si trop chaud on éteint
  {
    eteintChauff();
      return(1);
  }
  else{
    if (time < timeChauf) time = timeMaxiChauf +1 ; // si le compteur millis a dépassé sa limite, on reset
    if (time > timeChauf) time = time - timeChauf ; // si le temps maxi de marche du chauffage est dépassé on reset
    
    if (time > timeMaxiChauf){  // si le temps maxi d'allumage du chauffage est atteint, alors on reset
      bitWrite(lcd._bitString, 0, HIGH); //allumage led
    lcd.spiSendOut();
      
    eteintChauff();
      delay(40000);
      allumChauff(chauffOn);
      
            bitWrite(lcd._bitString, 0, LOW); //eteint led
    lcd.spiSendOut();
    }
    
    return(1); //sinon on chauffe encore
      }
}

// test si trop chaud
if (tempActu > tempOuv) {
  gestMot(OUV, 1, pasActuel);
  return(1);
}


// test si très froid on ferme et allume le chauffage
if (tempActu < tempFerm && tempActu < tempChauffOn){
  gestMot(FERM, 0, pasActuel);
  allumChauff(chauffOn);
  return(1);
  }

//   test si froid on ferme
if (tempActu < tempFerm) {
  if (hygroActu > hygroOuv){ // si trop d'hygro on tente d'ouvrir

    if (time - timeFermHygro > tempsFermHygroMini * 10 && tempsOuvHygroMax != 0 || timeFermHygro == 0){
      //on ouvre
      if (pasActuel != 1) { // si on est pas en position 1 on modifie l'ouverture
	gestMot(OUV, 2, pasActuel);
      }
      OuvHygro = 1;
      timeOuvHygro = time;
      return(1);
    }
    
    if (time - timeOuvHygro > tempsOuvHygroMax * 10){
      
      //On ferme
    gestMot(FERM, 0, pasActuel);
    OuvHygro = 0;
    timeFermHygro = time;
    	return(1);
    }

  }
  gestMot(FERM, 1, pasActuel);
  
  return(1);
}

return(1);  
}

/* Fonction gestion moteurs
//  pas = 0 : fermeture/ouverture totale.
//  pas = 1 : fermeture/ouverture d'un pas.
//  pas = 2 : ouverture fixe à 1 pas (quelque soit la position précédente).
// sens: OUV = ouverture   FERM = fermeture
*/
int gestMot(int sens, int pas, int & pasActuel){

  int timer = 0; // variable temporisation  
  int tmp = digitalRead(pinFerm);   // lit contact de fermeture
  Serial.println("gestMot");
  
// Ouverture d'un pas en plus  
if (sens == OUV && pas == 1 && pasActuel != nbPas){
  digitalWrite(pinPower, LOW);   // allumage alim.
delay(500);
  digitalWrite(pinSMoteurOuv, HIGH);    // moteur on pour ouvrir
tmp = digitalRead(pinOuv);   // lit contact d'ouverture

  while(tmp > 0 && timer < timoutMoteur && timer < pasOuv){
  delay(1);
  timer ++;
tmp = digitalRead(pinOuv);   // lit contact d'ouverture

  }
  digitalWrite(pinSMoteurOuv, LOW);    // moteur off pour ouvrir
  digitalWrite(pinPower, HIGH);   // arret alim.

  pasActuel ++;

  return(1);
}

//ouverture maxi
if (sens == OUV && pas == 0 && pasActuel != nbPas){
  int pasAction = nbPas - pasActuel ;
  int tempsAction = pasAction * pasOuv;
    digitalWrite(pinPower, LOW);   // allumage alim.
delay(500);
        digitalWrite(pinSMoteurOuv, HIGH);    // moteur on pour ouvrir
tmp = digitalRead(pinOuv);   // lit contact d'ouverture

  while(tmp > 0 && timer < timoutMoteur && timer < tempsAction){
  delay(1);
  timer ++;
tmp = digitalRead(pinOuv);   // lit contact d'ouverture

  }
        digitalWrite(pinSMoteurOuv, LOW);    // moteur off pour ouvrir
	
	pasActuel = nbPas;  digitalWrite(pinPower, HIGH);   // arret alim.
	return(1);
}

// ouverture à 1 pas
if (sens == OUV && pas == 2 && pasActuel != 1){
  if (pasActuel ==0){
        digitalWrite(pinPower, LOW);   // allumage alim.
delay(500);
        digitalWrite(pinSMoteurOuv, HIGH);    // moteur on pour ouvrir
	tmp = digitalRead(pinOuv);   // lit contact d'ouverture
 
	while(tmp > 0 && timer < timoutMoteur && timer < pasOuv){
  delay(1);
  timer ++;
tmp = digitalRead(pinOuv);   // lit contact d'ouverture

  }
	
        digitalWrite(pinSMoteurOuv, LOW);    // moteur off pour ouvrir
	    digitalWrite(pinPower, HIGH);   // arret alim.
	pasActuel = 1;
	return(1);
  }
  int pasAction = pasActuel - 1;
  int tempsAction = pasAction * pasOuv;
  digitalWrite(pinPower, LOW);   // allumage alim.
delay(500);
        digitalWrite(pinSMoteurFerm, HIGH);    // moteur on pour fermer l'excédent d'ouverture
tmp = digitalRead(pinFerm);   // lit contact de fermeture

  while(tmp > 0 && timer < timoutMoteur && timer < tempsAction){
  delay(1);
  timer ++;
tmp = digitalRead(pinFerm);   // lit contact de fermeture

  }
        digitalWrite(pinSMoteurFerm, LOW);    // moteur off pour fermer
	digitalWrite(pinPower, HIGH);   // arret alim.
	pasActuel = 1;
	return(1);
}

// Fermeture d'un pas en plus  
if (sens == FERM && pas == 1 && pasActuel != 0){
  digitalWrite(pinPower, LOW);   // allumage alim.
delay(500);
        digitalWrite(pinSMoteurFerm, HIGH);    // moteur on pour fermer

	tmp = digitalRead(pinFerm);   // lit contact de fermeture

  while(tmp > 0 && timer < timoutMoteur && timer < pasFerm){
  delay(1);
  timer ++;
tmp = digitalRead(pinFerm);   // lit contact de fermeture

  }
        digitalWrite(pinSMoteurFerm, LOW);    // moteur off pour fermer
	digitalWrite(pinPower, HIGH);   // arret alim.
	pasActuel --;
	return(1);
}
 
  //fermeture maxi
if (sens == FERM && pas == 0 && pasActuel != 0){
  int tempsAction = pasActuel * pasOuv;
  digitalWrite(pinPower, LOW);   // allumage alim.
delay(500);
        digitalWrite(pinSMoteurFerm, HIGH);    // moteur on pour fermer
tmp = digitalRead(pinFerm);   // lit contact de fermeture

  while(tmp > 0 && timer < timoutMoteur && timer < tempsAction){
  delay(1);
  timer ++;
tmp = digitalRead(pinFerm);   // lit contact de fermeture

  }
	digitalWrite(pinSMoteurFerm, LOW);    // moteur off pour fermer
digitalWrite(pinPower, HIGH);   // arret alim.
	pasActuel = 0;
	return(1);
}
  
}


// Fonction allumage chauffage
int allumChauff(int & chauffOn){
      bitWrite(lcd._bitString, 0, HIGH); //allumage led
    lcd.spiSendOut();
    
  digitalWrite(pinSChauff, HIGH);    // allumage le chauffage
  delay(30000);
 if (tempete == 0) gestMot(OUV,2, pasActuel);	// ouverture ventilation allumage sauf si mode tempête
  delay(120000);
  gestMot(FERM, 0, pasActuel); // fermeture
  chauffOn = 1 ; // enregistrement etat allumé

  //enregistrement heure allumage
  unsigned long time;
  time = millis();
timeChauf = time /60000 ; //temps en minutes
  
  mesures();

  NbAll ++; //compteur nombre allumages
  
      bitWrite(lcd._bitString, 0, LOW); //eteint led
    lcd.spiSendOut();
  
  return(1);
  
}

// Fonction arrêt chauffage
void eteintChauff(){
  digitalWrite(pinSChauff, LOW);    // éteint le chauffage
        unsigned long time = millis();
    time = time / 60000; //temps en minutes
  time = time - timeChauf; // temps de marche en minutes
  if (chauffOn == 1) HChauffOn += time ;
    chauffOn = 0 ;
  
}
// Fonction idle
int idle(){
  
int  tmp = 0;

while(tmp < tIdle){
delay(1);
  tmp ++;
  
    ClickEncoder::Button b = encoder->getButton();
  if (b != ClickEncoder::Open) {
    switch (b) {
      case ClickEncoder::Clicked:
	  return(3);  // vers affichage
	  break;
      case ClickEncoder::DoubleClicked:
	return(4);  // vers config
	break;
    }
  }
}

// si temps dépassé:
return(2);
}


// Fonction affichage / effacement des mesures Enregistrées

int affich(float & tempMini, float & tempMaxi, float & hygroMini, float & hygroMaxi){
    // allumage écran
//   	bitWrite(lcd._bitString, 0, HIGH); // led
    	bitWrite(lcd._bitString, 2, HIGH); //rétroéclairage écran

    lcd.spiSendOut();
    delay(10);
      lcd.begin(16, 2);
lcd.display();
  last = 0 ;
  value = 1 ;
  
int page = 1;
int lastpage = 0;

  // préparation des lignes  

//temps depuis dernier effacement
unsigned long timeSLastEff = millis() /1000 - lastEfface ; // temps en secondes depuis dernier effacement.
if (millis()/1000 < lastEfface) timeSLastEff = 0 ;
int lastEffJours = timeSLastEff / 86400 ;
int lastEffHeures = (timeSLastEff % 86400) / 3600;
int lastEffminutes = ((timeSLastEff % 86400) % 3600)/60;
int lastEffsecondes = ((timeSLastEff % 86400) % 3600)%60;

//heures chauffage
int HCJours = HChauffOn / 1440 ;
int HCHeures = (HChauffOn % 1440) / 60;
int HCMinutes = ((HChauffOn % 1440) % 60);


  int timer = 0;
  //   temporisation + lecture poussoir
  while(timer < 30000){
      value += encoder->getValue();
  
  if (value != last) {
    last = value;
    if (value > 4) value = 4 ;
    if (value < 1) value = 1 ;
    last = value;
    page = value;
  }
    
    if (page != lastpage){
     if (page == 1 ){	// affichage température
         lcd.clear();
         lcd.print("Temp C Min ");
	 lcd.print(tempMini);
	 lcd.setCursor(0, 1);
	 lcd.print(tempActu);
	 lcd.print(" / Ma ");
	 lcd.print(tempMaxi); 
     }
     
     if (page == 2 ){	//affichage hygrométrie
         lcd.clear();
         lcd.print("H% Mini ");
	 lcd.print(hygroMini);
	 lcd.setCursor(0, 1);
	 lcd.print(hygroActu);
	 lcd.print(" / Ma ");
	 lcd.print(hygroMaxi); 
     }

     if (page == 3 ){	//affichage temps chauffage
         lcd.clear();
         lcd.print("nb all: ");
	 lcd.print(NbAll);
	 lcd.setCursor(0, 1);
	 lcd.print("H: ");
	 lcd.print(HCJours);
	 lcd.print(" / ");
	 lcd.print(HCHeures); 
	 lcd.print(" / ");
	 lcd.print(HCMinutes);
     }
     
     if (page == 4 ){ // affichage temps depuis effacement
         lcd.clear();
         lcd.print("reset: ");
	 lcd.print(lastEffJours);
	 lcd.setCursor(0, 1);
	 lcd.print(lastEffHeures);
	 lcd.print(" / ");
	 lcd.print(lastEffminutes);
	 lcd.print(" / ");
         lcd.print(lastEffsecondes);
     } 
       
       
       
       
     lastpage = page ;
    }
    
        ClickEncoder::Button b = encoder->getButton();
  if (b != ClickEncoder::Open) {
    switch (b) {
      case ClickEncoder::Clicked:
	lcd.noDisplay();
	// clear screen for the next loop:
	lcd.clear();
	return(1);  // vers idle
	  break;
      case ClickEncoder::DoubleClicked:
	// clear screen for the next loop:
	lcd.clear();
	return(5);  // vers effacement
	break;
    }
  }
    
    delay(1);
    timer ++;

  }

//   éteint écran
 lcd.noDisplay();  
  // clear screen for the next loop:
  lcd.clear();
    return(1);  
 
}



//Fonction efface enregistrement de température/hygrométrie
int efface(){
      // allumage écran
//   	bitWrite(lcd._bitString, 0, HIGH); // led
    	bitWrite(lcd._bitString, 2, HIGH); //rétroéclairage écran

    lcd.spiSendOut();
    delay(10);
      lcd.begin(16, 2);
lcd.display();

  // efface écran
lcd.clear();

  lcd.setCursor(0,0);
      lcd.print("Effacer ?");
  lcd.setCursor(0,1);
      lcd.print("1clic=N ");
      lcd.print("2clic=O");

  int effaceO = 0;
   int timer = 0;
  //   temporisation + lecture poussoir
  while(timer < 10000 && effaceO == 0){
        ClickEncoder::Button b = encoder->getButton();
  if (b != ClickEncoder::Open) {
    switch (b) {
      case ClickEncoder::Clicked:
	lcd.noDisplay();
	// clear screen for the next loop:
	lcd.clear();
	return(1);  // vers idle
	break;
      case ClickEncoder::DoubleClicked:
	 effaceO = 1;  // vers effacement
	 break;
    }
    
  }
    
    delay(1);
    timer ++;
  }
   

// on efface les enregistrements si demandé
    if (effaceO == 1){
      tempMini = tempActu;
      tempMaxi = tempActu;
      hygroMini = hygroActu;
      hygroMaxi = hygroActu;
      NbAll = 0 ;
      lastEfface = millis() /1000;
      HChauffOn = 0 ;
      	
      bitWrite(lcd._bitString, 0, HIGH); // led
	lcd.spiSendOut();
	  lcd.clear();
	  lcd.print("Effacement");
	  lcd.setCursor(0, 1);
	  lcd.print("Ok");  
	  delay(1000);
	  bitWrite(lcd._bitString, 0, LOW); // led
	  lcd.spiSendOut();
	  lcd.noDisplay();
	  // clear screen for the next loop:
	  lcd.clear();
    }
 
 lcd.noDisplay();
 // clear screen for the next loop:
 lcd.clear();
    
return(1);
}
 
  
// Fonction Config
int config(){
  
    // allumage écran
//   	bitWrite(lcd._bitString, 0, HIGH); // led
    	bitWrite(lcd._bitString, 2, HIGH); //rétroéclairage écran

    lcd.spiSendOut();
    delay(10);
      lcd.begin(16, 2);
lcd.display();
  last = 0 ;
  value = 1 ;
  
int item = 1;
int lastitem = 0;
int page = 1;

   
  int timer = 0;
  //   temporisation + lecture poussoir
  while(timer < 60000){
      value += encoder->getValue();
  
  if (value != last) { // si l'encodeur a tourné
    last = value;
    if (value > 11) value = 11 ;
    if (value < 1) value = 1 ;
    last = value;
    item = value;
  }
    
    if (item != lastitem || page ==1){
         page = 0;
         lcd.clear();
	 lcd.print("> ");
         lcd.print(textMenuItem[item]);
	 lcd.setCursor(0, 1);
	 lcd.print(textMenuItem[item + 1]);

     lastitem = item ;
    }
    
        ClickEncoder::Button b = encoder->getButton();
  if (b != ClickEncoder::Open) {
    switch (b) {
      case ClickEncoder::Clicked:

	if (item < 9) {
	  page = editconfig(item);
	  if (page == 2) {	// si on a modifié le mode tempête on va vite fermer/ouvrir la serre
	  //   éteint écran
	  lcd.noDisplay();  
	  // clear screen for the next loop:
	  lcd.clear();
	  return(2);  
	  }
	}
	if (item == 9) page = initMoteurs(); // initmoteurs
	if (item == 10) page = resetDefaults(); // reset
	if (item == 11) { // sortie
	  //   éteint écran
	  lcd.noDisplay();  
	  // clear screen for the next loop:
	  lcd.clear();
	  return(1);  
	  break;
	}
    }
  }
    
    delay(1);
    timer ++;

  }

//   éteint écran
 lcd.noDisplay();  
  // clear screen for the next loop:
  lcd.clear();
    return(1);  
 
}


// fonction edit config
int editconfig(int item){
    encoder->setAccelerationEnabled(true);

  int valeur;
  int valeurMax;
  int valeurMini = 0;
  if (item == 1) {valeur = tempete; valeurMax = 1 ; }// on récupère la valeur de tempete
  if (item == 2) {valeur = tempOuv; valeurMini = tempFerm +1 ; valeurMax = 100 ; }// on récupère la valeur de tempOuv
  if (item == 3) {valeur = tempFerm; valeurMini = -20 ;  valeurMax = tempOuv -1; }// on récupère la valeur de tempFerm
  if (item == 4) {valeur = hygroOuv; valeurMax = 100; }// on récupère la valeur de hygroOuv
  if (item == 5) {valeur = tempsOuvHygroMax; valeurMax = 300 ; }// on récupère la valeur de tempsOuvHygroMax
  if (item == 6) {valeur = tempsFermHygroMini; valeurMax = 300 ; }// on récupère la valeur de tempsFermHygroMini
  if (item == 7) {valeur = tempChauffOn ; valeurMini = -20 ; valeurMax = tempChauffOff -1 ; }// on récupère la valeur de tempChauffOn
  if (item == 8) {valeur = tempChauffOff; valeurMini = tempChauffOn +1 ; valeurMax = 28; }// on récupère la valeur de tempChauffOff

  lcd.clear();
  lcd.print(textMenuItem[item]);
  lcd.setCursor(0, 1);
  lcd.print("Valeur: ");
  lcd.setCursor(9, 1);
  lcd.print(valeur);

  
  last = valeur ;
  value = valeur ;
   int timer = 0;
  //   temporisation + lecture poussoir
  while(timer < 60000){
      value += encoder->getValue();
  
  if (value != last) { // si l'encodeur a tourné
    last = value;
    if (value > valeurMax) value = valeurMax ;
    if (value < valeurMini) value = valeurMini ;
    last = value;
    valeur = value;
      lcd.clear();
  lcd.print(textMenuItem[item]);
  lcd.setCursor(0, 1);
  lcd.print("Valeur: ");
  lcd.setCursor(9, 1);
  lcd.print(valeur);
  }
    
        ClickEncoder::Button b = encoder->getButton();
  if (b != ClickEncoder::Open) {
    switch (b) {
      case ClickEncoder::Clicked:
	value = item;
	last = item ;
	  encoder->setAccelerationEnabled(false);
	return(1);
	break;
      case ClickEncoder::DoubleClicked:
	int retour = 1;
	if (item == 1 && valeur != tempete) retour = 2 ; // si tempete a changé on va fermer la serre de suite
	if (item == 1){ tempete = valeur ; cd.tempete = valeur; sauvegardeEEPROM();}
	if (item == 2){ tempOuv = valeur ; cd.tempOuv = valeur; sauvegardeEEPROM();}
	if (item == 3){ tempFerm = valeur ; cd.tempFerm = valeur; sauvegardeEEPROM();}
	if (item == 4){ hygroOuv = valeur ; cd.hygroOuv = valeur; sauvegardeEEPROM();}
	if (item == 5){ tempsOuvHygroMax = valeur ; cd.tempsOuvHygroMax = valeur; sauvegardeEEPROM();}
	if (item == 6){ tempsFermHygroMini = valeur ; cd.tempsFermHygroMini = valeur; sauvegardeEEPROM();}
	if (item == 7){ tempChauffOn = valeur ; cd.tempChauffOn = valeur; sauvegardeEEPROM();}
	if (item == 8){ tempChauffOff = valeur ; cd.tempChauffOff = valeur; sauvegardeEEPROM();}

	bitWrite(lcd._bitString, 0, HIGH); // led
	lcd.spiSendOut();
	  lcd.clear();
	  lcd.print("Valeur:");
	  lcd.setCursor(0, 1);
	  lcd.print(valeur);  
	  delay(1000);
	  bitWrite(lcd._bitString, 0, LOW); // led
	  lcd.spiSendOut();
	  value = item;
	  last = item ;
	    encoder->setAccelerationEnabled(false);
	    
	return(retour);
	break;
    }
  }
    
    delay(1);
    timer ++;

  }
    lcd.clear();
  encoder->setAccelerationEnabled(false);

}

// Fonction reset
int resetDefaults(){

    tempete = 0;	// mode tempete
    tempOuv = 40 ; // température d'ouverture
    tempFerm = 15 ; // température de fermeture
    hygroOuv = 80 ; // hygrométrie d'ouverture pour ventiler
    tempsOuvHygroMax = 5 ; //temps en minutes /10 d'ouverture maxi si trop d'hygro
    tempsFermHygroMini = 5; //temps en minutes /10 de fermeture mini si trop d'hygro
    tempChauffOn = 10 ; // température allumage chauffage
    tempChauffOff = 25 ; // température arret chauffage

    cd.tempete = tempete ;  // mode tempête désactivé/activé
    cd.tempOuv = tempOuv ; // température d'ouverture
    cd.tempFerm = tempFerm ; // température de fermeture
    cd.hygroOuv = hygroOuv ; // hygrométrie d'ouverture pour ventiler
    cd.tempsOuvHygroMax = tempsOuvHygroMax ; // nombre de minutes / 10 maxi où l'on reste ouvert si trop d'hygro
    cd.tempsFermHygroMini = tempsFermHygroMini ; // nombre de minutes / 10 où l'on reste fermé entre deux ouvertures si trop d'hygro
    cd.tempChauffOn = tempChauffOn ; // température allumage chauffage
    cd.tempChauffOff = tempChauffOff ; // température arret chauffage
    
    sauvegardeEEPROM();
    
    	bitWrite(lcd._bitString, 0, HIGH); // led
	lcd.spiSendOut();
		  lcd.clear();
	  lcd.print("Reset");
	  lcd.setCursor(0, 1);
	  lcd.print("Ok");
	delay(1000);
	bitWrite(lcd._bitString, 0, LOW); // led
	lcd.spiSendOut();
	return(1);
	
}


// fonction action tempete
int actionTempete() {
      unsigned long time = millis();
    time = time / 60000; //temps en minutes

    if (hygroActu < hygroOuv && OuvHygro == 1){ // si on était ouvert pour trop d'hygro et que l'hygro a baissé
      OuvHygro = 0;
      timeFermHygro = time;
    }

  gestMot(FERM, 0, pasActuel); // ferme la serre
  mesures();

  //test si chauffage en marche
if (chauffOn ==1){
  if (tempActu > tempChauffOff) //si trop chaud on éteint
  {
      eteintChauff();
      return(1);
  }
  else{
    if (time < timeChauf) time = timeMaxiChauf +1 ;
    if (time > timeChauf) time = time - timeChauf ;
    
    if (time > timeMaxiChauf){  // si le temps maxi d'allumage du chauffage est atteint, alors on reset
      bitWrite(lcd._bitString, 0, HIGH); //allumage led
    lcd.spiSendOut();

    eteintChauff();
      delay(40000);
      allumChauff(chauffOn);
      
            bitWrite(lcd._bitString, 0, LOW); //eteint led
    lcd.spiSendOut();
    }
    
    return(1); //sinon on chauffe encore
      }
}

 // test si très froid on allume le chauffage
if (tempActu < tempFerm && tempActu < tempChauffOn){
  allumChauff(chauffOn);
  return(1);
  } 
  
  return(1);
}


/** Charge le contenu de la mémoire EEPROM dans la structure */
void chargeEEPROM() {

  // Lit la mémoire EEPROM
  EEPROM.get(0, cd);
  
  // Détection d'une mémoire non initialisée
  byte erreur = cd.magic != STRUCT_MAGIC;

  // Valeurs par défaut struct_version == 0
  if (erreur) {
    // Valeurs par défaut pour les variables de la version 0
    cd.tempete = tempete ;  // mode tempête désactivé/activé
    cd.tempOuv = tempOuv ; // température d'ouverture
    cd.tempFerm = tempFerm ; // température de fermeture
    cd.hygroOuv = hygroOuv ; // hygrométrie d'ouverture pour ventiler
    cd.tempsOuvHygroMax = tempsOuvHygroMax ; // nombre de minutes / 10 maxi où l'on reste ouvert si trop d'hygro
    cd.tempsFermHygroMini = tempsFermHygroMini ; // nombre de minutes / 10 où l'on reste fermé entre deux ouvertures si trop d'hygro
    cd.tempChauffOn = tempChauffOn ; // température allumage chauffage
    cd.tempChauffOff = tempChauffOff ; // température arret chauffage
    etat = 10; // vers init moteurs
    
      // Sauvegarde les nouvelles données
  sauvegardeEEPROM();
  }
  
  // Valeurs par défaut struct_version == 1
//   if (cd.struct_version < 1 || erreur) {

    // Valeurs par défaut pour les variables de la version 1
//     cd.valeur_2 = 13.37;
//   }

  // Valeurs par défaut pour struct_version == 2
//   if (cd.struct_version < 2 || erreur) {

    // Valeurs par défaut pour les variables de la version 2
//     strcpy(cd.valeur_3, "Hello World!");
//   }

// charge les valeurs dans les variables programme
    tempete = cd.tempete ;  // mode tempête désactivé/activé
    tempOuv = cd.tempOuv ; // température d'ouverture
    tempFerm = cd.tempFerm ; // température de fermeture
    hygroOuv = cd.hygroOuv ; // hygrométrie d'ouverture pour ventiler
    tempsOuvHygroMax = cd.tempsOuvHygroMax ; // nombre de minutes / 10 maxi où l'on reste ouvert si trop d'hygro
    tempsFermHygroMini = cd.tempsFermHygroMini ; // nombre de minutes / 10 où l'on reste fermé entre deux ouvertures si trop d'hygro
    tempChauffOn = cd.tempChauffOn ; // température allumage chauffage
    tempChauffOff = cd.tempChauffOff ; // température arret chauffage
    ouvTime = cd.ouvTime ;
    fermTime = cd.fermTime ;
    pasOuv = cd.pasOuv ;
    pasFerm = cd.pasFerm ;

}


/** Sauvegarde en mémoire EEPROM le contenu actuel de la structure */
void sauvegardeEEPROM() {
  // Met à jour le nombre magic et le numéro de version avant l'écriture
  cd.magic = STRUCT_MAGIC;
  cd.struct_version =  STRUCT_VERSION;
  EEPROM.put(0, cd);
}

